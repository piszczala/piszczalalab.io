Title: Projects

## LlameDL

<p align="center"><img src="https://github.com/westscz/llameDL/raw/master/media/logo.png"/></p>

[Link do projektu](https://github.com/westscz/llameDL)

Aplikacja CLI do konwersji linków z YouTube do plików MP3 oraz ich odpowiedniego tagowania.


## Automation playground

[Link do projektu](https://github.com/westscz/Automation_playground)

Piaskownica ze skryptami do automatzacji różnych powtarzalnych zadań

## Architektura Komputerow

[Link do projektu](https://gitlab.com/piszczala/ArchitekturaKomputerow2)

Repozytorium pełne skryptów assemblerowych z organizowanych na 
Politechnice Wrocławskiej zajęć "Architektura Komputerów 2"

## Piszczala - Blog techniczny

[Link do projektu](https://gitlab.com/piszczala/piszczala.gitlab.io)

Ten blog w formie kodu. Wszystkie tekty zapisane są w Markdownie, sam blog generowany jest
za pomocą biblioteki Pelican, generatora statycznych stron napisanego w Pythonie.

## Repozytoria

Linki do repozytoriów na których trzymam swój kod

[Github](https://github.com/westscz)
[Gitlab](https://gitlab.com/piszczala)
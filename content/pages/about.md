Title: About
Header_Cover: theme/images/about-bg.jpg

**Tech Blog** powstał jako eksperyment oraz jeden z celów na przyszłe lata. To początek podróży w zdobywaniu wiedzy i dzieleniu się nią z innymi.
Próba okiełznania technologii, tworzenia projektów i pojawiających się za tym problemów.

Artykuły które są publikowane na tym blogu to swoista próba przelania wiedzy którą posiadłem w zrozumiałe zlepki słów
aby dać coś od siebie i przekazać tę wiedzę innym. Chcę aby publikowane przeze mnie artykuły były poparte konkretną
literaturą, gdyż wyznaję zasadę, że zawsze powinno się czerpać wiedzę z wielu źródeł.

Personalnie jestem fanem automatyzowania nudnych rzeczy aby mieć więcej czasu na to co lubię.

## Prezentacje

### Wroc.py - Sierpień 2019 - Backend dla bystrzaków

* [Slajdy](https://piszczala.pl/slides/002__backend_for_dummies/)
* [Tekst](https://)

### Wroc.py - Marzec 2019 - Od problemu do aplikacji czyli jak automatyzować zadania

* [Slajdy](https://piszczala.pl/slides/001__problem-system-automation/)
* [Tekst]([https://](https://piszczala.pl/posts/2019/Mar/wroc_py_54/))

Title: Markdown syntax
save_as: cheatsheet/markdown.html

## Headings

# Heading level 1

## Heading level 2

### Heading level 3

#### Heading level 4

##### Heading level 5

###### Heading level 6

## Text formating

_Italic_ _Italic_

**Bold** **Bold**

**_Boldalic_** **_Boldalic_**

~~strikethrough~~

Math $e=mc^2$

`:unicorn:` :unicorn:

    This is in first line  <- Two spaces
    And this is in second

This is in first line  
And this is in second

    > Blockquote

> Blockquote

> Blockquote line 1  
> Blockquote line 2
> Blockquote line 3
>
> > Omg  
> > This is awesome
>
> - Element 1
> - Element 2

## Line

    ---
    ***

---

---

## List

    1. One
    2. Two

    1) Three
    2) Four

    * Five
    * Six

    - Seven
    - Eight

1. One
2. Two
   3. Two and half

1) Three
1) Four

- Five
- Six
  - Six and half

* Seven
* Eight

## Code

    `Inline code`

`Inline code`

        import code
                print(code.__name__)

.

    import code
    print(code.__name__)

```python
import code
print(code.__name__)
```

## Todo list

- [ ] Todo not done
- [x] Todo done

## Image

    ![Image](markdown_mark_white.svg)

![Image](/images/profil.png)

## Reference

    Quote[^1]
    ---
    [^1]: https://Bibliographylinkorsomething.com`

Quote[^1]

[^1]: https://reference.com

## Links

    [Name](https://www.url.com)
    <https://www.url.com>

[Name](https://www.url.com)

<https://www.url.com>

---

##### Guides

[Markdown Guide](https://www.markdownguide.org/)  
[CommonMark](https://commonmark.org/)  
[GitHub Flavored Markdown](https://github.github.com/gfm/)
[GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)  
[MDX](https://mdxjs.com/)

##### Linter

[Prettier](https://prettier.io/)

Title: PyCharm
save_as: cheatsheet/pycharm.html

`CTRL + SHIFT + A` - Actions popup

`CTRL + TAB` - Switcher

`CTRL + E` - Recent Files

`CTRL + Shift + E` - Recently Edit Files

`CTRL + K` - Git commit

`CTRL + Shift + K` - Push committed changes

---

Footnote

<https://medium.com/@ckalpit/become-a-pro-user-of-pycharm-or-any-other-intellij-ides-part-1-1827c556d6fa>

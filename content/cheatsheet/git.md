Title: GIT Cheatsheet
save_as: cheatsheet/git.html

## Rename a local and remote branch [^1]

[^1]: https://multiplestates.wordpress.com/2015/02/05/rename-a-local-and-remote-branch-in-git/

## Revert last commit [^2],[^3]

[^2]: https://dev.to/isabelcmdcosta/how-to-undo-the-last-commit--31mg
[^3]: http://christoph.ruegg.name/blog/git-howto-revert-a-commit-already-pushed-to-a-remote-reposit.html

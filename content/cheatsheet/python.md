Title: Python cheatsheet
save_as: cheatsheet/python.html

## JSON file

### Writing JSON to a file [^1]

```python
import json

data = {'people': {'name': 'Scott', 'website': 'stackabuse.com', 'from': 'Nebraska'} }

with open('data.txt', 'w') as outfile:
    json.dump(data, outfile)
```

### Reading JSON from a file [^1]

```python
import json

with open('data.txt') as json_file:
    data = json.load(json_file)
    for p in data['people']:
        print(p)
```

## Context Managers [^2]

### Using

```python
with something_that_returns_a_context_manager() as my_resource:
    do_something(my_resource)
    ...
    print('done using my_resource')
```

### Custom context manager as class

```python
class File():

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        self.open_file = open(self.filename, self.mode)
        return self.open_file

    def __exit__(self, *args):
        self.open_file.close()

files = []
for _ in range(10000):
    with File('foo.txt', 'w') as infile:
        infile.write('foo')
```

### Custom context manager as decorated function

```python
from contextlib import contextmanager

@contextmanager
def open_file(path, mode):
    the_file = open(path, mode)
    yield the_file
    the_file.close()

files = []

for x in range(100000):
    with open_file('foo.txt', 'w') as infile:
        files.append(infile)

for f in files:
    if not f.closed:
        print('not closed')
```

## Dict

### Default dict [^4]

## Print data

### Pretty Print [^3]

```python
import json
import pprint

with open('filename.txt', 'r') as f:
    data = f.read()
    json_data = json.loads(data)

pprint.pprint(json_data)
```

or

```python
import json
your_json = '["foo", {"bar":["baz", null, 1.0, 2]}]'
parsed = json.loads(your_json)
print(json.dumps(parsed, indent=4, sort_keys=True))
```

---

[^1]: https://stackabuse.com/reading-and-writing-json-to-a-file-in-python/
[^2]: https://jeffknupp.com/blog/2016/03/07/python-with-context-managers/
[^3]: https://stackoverflow.com/questions/12943819/how-to-prettyprint-a-json-file
[^4]: https://www.accelebrate.com/blog/using-defaultdict-python/

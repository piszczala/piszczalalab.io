Title: Auto Case - Agregowanie materiałów z różnych źródeł
Slug: auto_case_01
Date: 2019-05-19 15:15
Category: autocase
Tags: autocase, ifttt, pocket
Header_copyright: Samuel Zeller (https://unsplash.com/photos/G_xJrvHN9nk)
Header_Cover: images/posts/003/header.jpeg
Twitter_Image: images/posts/003/header.jpeg

Dużo swojego czasu poświęcam na tematy związane z automatyzacją. Często jednak gdy mówię o autmatyzacji w prywatnym życiu
słyszę pytania ale jak, ale po co. Tym samym chciałbym rozpocząć kategorie wpisów Auto Case w których tłumaczyć będę
przejście przez moją zasadę `problem, system, automatyzacja`.

Skąd Auto Case? Etymologia jest dosyć prosta. Jest to połączenie `Use Case` z pojęciem automatyzacji.
Takie dosłowne "przypadki automatyzacji".

## Problem

Swego czasu gdy zacząłem studia, zacząłem także dużo więcej czytać i zbierać różnych materiałów związanych
z tym czym się interesuje, wtedy głównie programowaniem. Ale gdzie tu problem? Otóż z czasem ilość źródeł
zaczęła się rozrastać, i wraz z nią czas potrzebny na ich przeglądanie. Mimo to za nic nie chciałem odrzucać już wybranych
źródeł. Problem który tutaj rozwiążemy opiera się o zbieranie materiałów do jednego punktu gdzie zostaną one
przejrzane i odczytane.

Aby łatwiej tłumaczyć system oraz automatyzacje ustalmy kilka rekwizytów:

- Źródła: To blogi, serwisy społecznościowe, etc.
- Materiały: To linki, wpisy, dokumenty
- Liczba źródeł w moim przypadku to około 50 w początkowej fazie (aktualnie ok. 500).

## System

System miał kilka iteracji z etapowym zwiększaniem automatyzacji. Warto zwrócić uwagę iż cały proces
w moim przypadku jest rozłożony w czasie około siedmiu lat. Patrząc na, iż to przyzwyczajenie przez ten czas
pozostało w życiu, utworzenie systemu i automatyzacja była tu bardzo potrzebna i przydatna.

Początkowy system był w pełni manualny i czasochłonny. W pierwszej kolejności wchodziłem na każde źródło z osobna
sprawdzając czy pojawił się może nowy wpis. Gdy znalazłem coś ciekawego, link lądował w zakładkach
i po przejrzeniu wszystkich źródeł zabierałem się do czytania wybranych wpisów

<p align="center"><img alt="Stage 1" src="../../../../images/posts/003/stage_01.svg"></p>

## Automatyzacja

### Wykorzystanie RSS

Gdy odkryłem przypadkiem dzięki jednemu z wpisów co to jest RSS oszalałem na jego punkcie. Co prawda poznałem to hasło
gdy według internetu RSS był w odwrocie ale nie przeszkodziło mi to na wykorzystanie go w kolejnym etapie. Dzięki
nim, zautomatyzowałem jeden z elementów systemu - już nie musiałem ręcznie sprawdzać każdego źródła osobno w poszukiwaniu
nowych wpisów. Jako czytnik RSS wykorzystuje po dziś dzień [Feedly](http://feedly.com) który idealnie trafia w moje
potrzeby.

Flow systemu po tej zmianie pozostało podobne, przeglądając nowe wpisy na Feedly wrzucałem co interesujące materiały
do zakładek. Dzięki temu, oraz synchronizacji zakładek miałem dostęp do materiałów w domu, pracy oraz na telefonie.

<p align="center"><img alt="Stage 2" src="../../../../images/posts/003/stage_02.svg"></p>

Koszt tej automatyzacji jest niewielki. Około godziny na założenie konta oraz dodanie wszystkich źródeł z zakładek
do Feedly. W tym momencie można importować je z pliku więc można jeszcze bardziej przyspieszyć ten etap.
Jednakże co najważniejsze, było to jednorazowe spędzenie czasu które od pierwszej chwili pozwoliło odzyskać pewien czas.

W ten sposób udało się także obsłużyć media społecznościowe takie jak reddit czy wykop, które posiadają RSS dla
swych tablic/tagów. Dzięki temu, ograniczyłem także czas spędzany na tych portalach do minimum.

### Dodatkowe źródła

Wciąż pozostała kwestia innych źródeł które nie mają kanału RSS. O ile będąc przy komputerze sprawa była prosta,
i każdy twitt czy wpis na facebooku mogłem wrzucić bez problemu do zakładek to przeglądając te serwisy na telefonie
było to nie możliwe. Tak, Twitter oraz Facebook posiadają możliwość zapisywania materiałów, ale wtedy znów dochodzi
kwestia przeglądania na komputerze kolejnych miejsc gdzie mogłem coś zapisać aby wyciągnąć to do zakładek.

Można by do tego napisać skrypt który pobierał by informacje z każdej witryny i wrzucał w jedno miejsce.
Na szczęście w tym przypadku nasza automatyzacja obejdzie się wciąż bez kodu.

### If this, then that

Istnieje w internecie serwis [IFTTT](https://ifttt.com) który pozwala na tworzenie reakcji na zintegrowanych z systemem
platformach. Gdy powstał ten portal, używałem go głównie do backupu zdjęć z serwisów. Jednak teraz pomyślałem, że
to świetne miejsce do wykorzystania go. Wystarczy stworzyć tzw. Applet, zdefiniować co ma się wydarzyć aby się uruchomił,
i co następnie ma zrobić.

Jedyne co mi wtedy pozostało to wybrać gdzie zbierać wszystkie treści. Dorzucanie każdego wpisu do arkusza nie było by
zbyt użyteczne. Z grupy różnych aplikacji pozwalających na gromadzenie materiałów wybrałem ostatecznie
[Pocket](https://getpocket.com) który mogłem zintegrować w IFTTT, a dodatkowo miał prosty interfejs.

<p align="center"><img alt="IFTTT Dashboard" src="../../../../images/posts/003/ifttt.svg"></p>

Triggery dla moich appletów w tym przypadku są dosyć proste, jeżeli coś polubię bądź zapiszę, zapisuję to na Pocket.
Dzięki temu mogę dość szybko przebrnąć na telefonie przez nowe materiały każdego poranka i wszystko ląduje w Pocket
do którego dostęp posiadam z poziomu telefonu oraz komputera.

<p align="center"><img alt="Stage 3" src="../../../../images/posts/003/stage_03.svg"></p>

Koszt tego etapu tak jak i poprzedniego jest niewielki. Rejestracja w platformie plus integracja z portalami z którymi
chcemy współpracować oraz napisanie odpowiedniego apletu bądź znalezienie już istniejącego w bazie. Po zrobieniu tego
reszta dzieje się już automatycznie od tego momentu. Co ważniejsze, udało nam się zautomatyzować już masę pracochłonnej
manualnej pracy bez pisania ani jednej linii kodu!

### Czegoś tu brakuje...

Udało się zautomatyzować kawał systemu ale przy nowym rozwiązaniu wciąż brakuje dodawania zakładek do przeglądarki.
I tutaj już sięgnałem po Pythona, napisałem skrypt który przenosi materiały zapisane w pocket do zakładek w Chrome.
[Skrypt](https://github.com/westscz/AutomateBoringStuff/blob/master/chrome_pocket.py) mam ustawiony w cronie
na prywatnym komputerze, więc mam pewność, że pobiorę wszystko do zakładek tylko wtedy gdy faktycznie jestem w domu
i mam czas by to przejrzeć. W innym przypadku wszystkie materiały dostępne mam w Pocket.

<p align="center"><img alt="Stage 4" src="../../../../images/posts/003/stage_04.svg"></p>

Koszt tutaj to znalezienie biblioteki do rozmawiania z pocket, napisanie skryptu oraz testy skryptu.
Po około 2 godzinach skrypt był gotowy do użytku.

## Podsumowanie

Tak o to przeszliśmy od problemu, do ustalenia systemu aż po iteracyjną implementacje kolejnych elementów
automatycznych w systemie. Nie jest to problem w pełni automatyzowalny, jednakże udało się do minimum ograniczyć
pracę ludzką w obszarach które nie są twórcze.

Ten przypadek pokazuje, że wcale nie trzeba posiadać umiejętności programowania aby móc automatyzować swoje zadania.
I to chyba najważniejsze, co chciałem przekazać w tym artykule. Wystarczą odpowiednie narzędzia i pomysł jak ich użyć.

Automatyzowanie elementów czasochłonnych, powtarzalnych pozwoliło nam odzyskać sporo czasu. Dzięki temu w moim przypadku
mogłem poszerzać ilość źródeł bez strachu o dodawanie sobie ręcznej nudnej pracy przy ich obsłudze.

Title: DevCrawler #0
Date: 2018-12-10 19:00
Category: DevCrawler
Tags: devcrawler
Slug: devcrawler-1
Authors: Jarosław Piszczała
Summary: Prolog do serii zestawień linków pod nazwą 'DevCrawler'
Status: draft
Time: 2 min

Abstract: Prolog do serii zestawień linków pod nazwą 'DevCrawler'

**P**omysł na takie zestawienie linków wziął się z mojego codziennego przeglądu prasy oraz z mikrobloga.

**P**rzez lata jak kret przedzierałem się przez masę różnych artykułów i blogów, starając się czerpać z tego jak najwięcej wiedzy jak tylko mogę. Z czasem ilość serwisów doszła do tego etapu, że należało się rozejrzeć za metodą prostego śledzenia nowo dodanych wpisów.

**T**ak natrafiłem na RSS oraz serwis Feedly który jest aktualnie jednym z moich głównych narzędzi jeśli chodzi o przegląd prasy. Trafiając na ludzi których mogą te linki interesować zacząłem rozsyłać co ciekawszy artykuł znajomym, jednakże okazało się to mało efektywne. Spójrz prawdzie w oczy, rozsyłanie ręcznie linków osobno do kilku osób nie możemy uznać za efektywne. Wtem naszedł mnie pomysł aby tworzyć własne zestawienia linków tak jak robią to niektórzy w szeroko pojętym internecie, gdzie dla przykładu mogę podać 'unknowNews' Jakuba Mrugalskiego, 'Wednewsday' Grzegorza Kotfisa czy proste zestawienia newsów użytkowników mikrobloga - qarmin (godot), normanos (web)

**N**a dziś moje Feedly zawiera ponad 550 źródeł, i właśnie na ich podstawie tworzone będą zestawienia DevCrawler! Gorąco zapraszam do ich śledzenia, a jeżeli sami natraficie na interesujący artykuł bądź blog, przyjmę go z ochotą aby poszerzać moją i Waszą wiedzę.

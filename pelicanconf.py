#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Jarosław Piszczała'
SITENAME = u'Piszczała'
SITESUBTITLE = u'Technical Blog'
SITEURL = ''
RELATIVE_URLS = True

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Warsaw'
LOCALE = " et_EE.UTF-8 "
DEFAULT_LANG = u'pl_PL'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (
    ('envelope', 'mailto:jaroslaw.piszczala@gmail.com'),
    ('linkedin', 'https://www.linkedin.com/in/jaroslaw-piszczala'),
    ('github', 'https://github.com/westscz'),
    ('gitlab', 'https://gitlab.com/piszczala'),
    ('twitter', 'https://twitter.com/jpiszczala'),
    ('goodreads-g', 'https://www.goodreads.com/user/show/87975661-jaros-aw-piszcza-a'),
)

FAVICON = 'images/favicon.ico'
DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = "theme"

MENUITEMS = [('Articles', '/archives.html'),
             ('Categories', '/categories.html'),
             ('Resume', '/files/cv.pdf'),
             ('Projects', '/pages/projects'),
             ('About Me', '/pages/about'),
             ('RSS', '/feeds/all.atom.xml')]

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

STATIC_EXCLUDE_SOURCES = False
STATIC_PATHS = ['images', 'files', 'slides']

WITH_FUTURE_DATES = True

PLUGIN_PATHS = ['plugins']
PLUGINS = ['read_time']

READ_TIME = {
    'default': {
        'wpm': 120,
        'plurals': ['minut', 'minut']
    }
}

PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{slug}/index.html'

DISABLE_CUSTOM_THEME_JAVASCRIPT = True

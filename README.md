<div align="center">
  <h1>piszczala.gitlab.io ‍‍💻</h1>
  <strong>Technical blog</strong>
</div>

Welcome to the piszczala.gitlab.io codebase.

## Article properties

This properties must be added as first lines of article

    Title: My super title
    Slug: my-super-post
    Time: 10 min
    Date: 2010-12-03 10:20
    Modified: 2010-12-05 19:30
    Category: Python
    Tags: pelican, publishing
    Authors: Alexis Metaireau, Conan Doyle
    Summary: Short version for index and feeds
    Header_Cover: /images/posts/super-title/cover.png
    Og_Image: http://example.com/facebook_cover.png
    Twitter_Image: http://example.com/twitter_cover.png

    This is the content of my super blog post.

To get article estimate reading time you can use: [READ-O-METER](http://niram.org/read/), [ReadTime](http://readtime.eu/),
https://alireza.codes/how-to-calculate-read-time-in-your-posts.html

for tags you can use [Word Counter](https://wordcounter.io/)

## Draft

If article is not ready to publish, you can just make it as a draft.  
Add `Status: draft` to article properties. Draft article will be still created but will be not visible as public.
All _draft_ articles will be rendered to /draft directory.

## Markdown formatting

Markdown formatting is done using [Prettier](https://prettier.io/)

To install you need `yarn` or `npm` package manager.

    yarn global add prettier
    or
    npm install --global prettier

Then use

    prettier "**/*.md"

## Optimize

You can create SVG screenshots with:

https://www.checkbot.io/article/web-page-screenshots-with-svg/

Other images try to optimize with:

https://github.com/victordomingos/optimize-images

    optimize-images ./

or

https://imageoptim.com/mac

## Cheatsheet

My simple cheatsheets are available on piszczala.gitlab.io/cs.html

Available now:

- Markdown
- Python
- Git

## Theme

Theme is custom [Pelican clean blog](https://github.com/gilsondev/pelican-clean-blog)
created by [Gilson Filho](https://github.com/gilsondev)
which is based on [Bootstrap Clean Blog](https://startbootstrap.com/template-overviews/clean-blog/)

### Color

Color is based on PANTONE color of the year 2016 - [Serenity & Rose Quartz](https://www.pantone.com/color-intelligence/color-of-the-year/color-of-the-year-2016-color-standards)

## Engine

Blog is powered by [Pelican](), and here is [Pelican documentation](http://docs.getpelican.com/en/3.6.3/index.html)

## Thanks to

Plugin developers:

Read Time - https://github.com/deepakrb/Pelican-Read-Time

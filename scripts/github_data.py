from github import Github, NamedUser, Repository

repos = ['llameDL', 'Maxdila']

gh = Github()
user: NamedUser = gh.get_user('westscz')
for repo in [r for r in user.get_repos() if r.name in repos]:
    print(repo.html_url) #URL
    print(repo.name) #Project name
    print(repo.description) #Short description
    print(repo.get_readme().decoded_content) #One section as text
    print(repo.get_contents('logo.png').download_url) #LOGO to download